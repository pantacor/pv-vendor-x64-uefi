#!/bin/sh

set -e

PATH=$PATH:/usr/local/bin
export PATH

cmd=$0
mntpoint=${1:-/storage}

type=`echo $cmd | sed -e 's/.*\.//g'`
tool=`echo $cmd | sed -e 's/.*\///g;s/\..*//g'`

if [ -z "$type" ]; then
	echo "{ \"error\": \"ERROR: must be called with filename ending with btool type: login\" }"
	exit 1
fi

chroot=/chroot.$type
chroottmp=/tmp/chroot.$type
chrootconfig=$chroot/tmp/pv/config

mkdir -p $chroot || true
mkdir -p $chroottmp || true

mount /btools/toolbox.$tool.squashfs $chroot
mount -t proc proc $chroot/proc
mount -t devtmpfs dev $chroot/dev
mount -o bind $chroottmp $chroot/tmp

mkdir -p $chroot/$chroottmp || true
mkdir -p $chrootconfig || true

mount -o bind /storage/config $chrootconfig
mount -o bind /etc/resolv.conf $chroot/etc/resolv.conf

cat > $chroot/$chroottmp/login <<EOF
#!/bin/sh

getsetting() {
        key=\$1
        val=\`cat /tmp/pv/config/pantahub.config | grep \$1  | sed -e "s/.*=//"\`
        ovrwrt=\`cat /proc/cmdline | grep "[^a-zA-Z0-9]\$1=" | sed -e "s/.*\$1=//" | sed -e "s/\ .*$//g"\`
        if [ -n "\$ovrwrt" ]; then
                val=\$ovrwrt
        fi
        echo \$val
}

host=\`getsetting creds.host\`
port=\`getsetting creds.port\`
user=\`getsetting creds.prn\`
pass=\`getsetting creds.secret\`

curl -s -XPOST -H 'Content-Type: application/json' \
        --data "{ \"username\": \"\$user\", \"password\": \"\$pass\" }" \
        https://\$host:\$port/auth/login
exit \$?
EOF

chmod a+x $chroot/$chroottmp/login
/sbin/chroot $chroot $chroottmp/login $@

umount $chroot/etc/resolv.conf
umount $chrootconfig
umount $chroot/tmp
umount $chroot/dev
umount $chroot/proc
umount $chroot
rm -rf $chroottmp
exit 0

